import Vue from 'vue'
import Vuex from 'vuex'
//import * as getters from './getters'
//import * as mutations from './mutations'
import todoApp from './modules/todoApp.js'
Vue.use(Vuex);

// const storage = {
//     fetch() {
//         const arr = [];
//         if (localStorage.length > 0) {
//             for (let i = 0; i < localStorage.length; i++) {
//                 if (localStorage.key(i) !== "loglevel:webpack-dev-server") {
//                     arr.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
//                 }
//             }
//         }
//         return arr;
//     }
// }

export const store = new Vuex.Store({
  modules: {
    todoApp
  }
    // state: {
    //     headerText: 'TODO it!!!!',
    //     todoItems:storage.fetch()
    // },
    // getters,
    // getters: {
    //   storedTodoItems(state){
    //     return state.todoItems;
    //   }
    // },
    //mutations
    // mutations: {
    //   addOneItem(state, totoItem) {
    //     if (totoItem !== "") {
    //         let obj = { completed: false, item: totoItem };
    //         localStorage.setItem(totoItem, JSON.stringify(obj));
    //         state.todoItems.push(obj);
    //     }
    //   },
    //   removeTodo(state, payload){
    //     localStorage.removeItem(payload.todoItem.item);
    //     state.todoItems.splice(payload.index, 1);
    //   },
    //   toggleComplete(state, payload){
    //     state.todoItems[payload.index].completed = !state.todoItems[payload.index].completed;
    //     localStorage.removeItem(payload.todoItem.item);
    //     localStorage.setItem(payload.todoItem.item, JSON.stringify(payload.todoItem));
    //   },
    //   clearTodo(state){
    //     localStorage.clear();
    //     state.todoItems = [];
    //   }
    // }
});