## Todo List
### ■ 컴포넌트 설계
> - TodoHeader, TodoInput, TodoList, TodoFooter 4개 컴포넌트로 구성
### ■ 각 컴포너트에서 기능 구현
> - TodoHeader 헤더 정보 노출
> - TodoInput 할일 로컬스토리지에 저장
> - TodoList 할일 로컬스토리지에서 가져와 목록 구현
> - TodoFooter 로컬 스토리지 ClearAll 기능 구현