## 사용자 경험 개선
### ■ Modal 팝업 적용
> - https://vuejs.org/v2/examples/modal.html
### ■ Slot
> - https://kr.vuejs.org/v2/guide/components-slots.html
### ■ Transition
> - https://vuejs.org/v2/guide/transitions.html#ad