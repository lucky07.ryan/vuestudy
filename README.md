# VueStudy
## 1일차
### 개발 환경 설정
> ### ■ 프로그램 설치 (필수)
> 1. Chrome (https://www.google.com/intl/ko/chrome/)
> 2. Visual Studio Code (https://code.visualstudio.com/download)
> 3. Node.js (https://nodejs.org/ko/download/)
> 4. Vue.js Devtools (https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)
> ### ■ VsCode 플러그인 설치 및 설정 (선택)
> 1. Vetur : Vue.js 코드 자동완성 플로그인
> 2. Night Owl : 소스 테마 플러그인
> 3. Material Icon Theme : 아이콘 테마 플러그인
> 4. Live Server : 소스 코드 실행을 위한 플러그인
> 5. ESLint : JavaScript Linting 유틸리티
> 6. Prettier : 정해진 규칙에 따라 자동으로 코드 스타일을 정리해주는 도구
> 7. Auto Close Tag : 태그를 자동으로 닫게 해주는 플로그인
> ### ■ 뷰 개발자 도구 소개 및 실행 방법
> ![Vue devtools](https://gitlab.com/lucky07.ryan/images/raw/master/vue_devtools.png)
## 2일차
### Vue.js 소개
> ### ■ MVVM 모델에서의 Vue
> ![MVVM 모델](https://gitlab.com/lucky07.ryan/images/raw/master/vue_intro.png)
> ### ■ 기존 웹 개발 방식(HTML, Javascript)
> ### ■ Reactivity 구현
> - Object.defineProperty() API 문서 (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)
> ### ■ Reactivity 코드라이브러리화 하기
> - 즉시 실행 함수 MDN 문서 링크 (https://developer.mozilla.org/ko/docs/Glossary/IIFE)
> ### ■ Hello Vue.js와 뷰 개발자 도구
## 3일차
### 인스턴스
> ### ■ 인스턴스 소개
> ![인스턴스 소개](https://gitlab.com/lucky07.ryan/images/raw/master/instance.png)
> ### ■ 인스턴스와 생성자 함수
> - 자바스크립트에서 함수를 이용해 인스턴스를 생성하는 방법은 생성자 함수를 이용
> - MDN 생성자 함수 설명 문서 (https://developer.mozilla.org/ko/docs/Web/JavaScript/Guide/Obsolete_Pages/Core_JavaScript_1.5_Guide/Creating_New_Objects/Using_a_Constructor_Function)
> - MDN Prototype 설명 문서서 (httpshttps://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Object/constructor)
> ### ■ 인스턴스 옵션 속성
> - el 옵션: 인스턴스가 그려지는 화면의 시작점 (특정 HTML 태그)
> - template 옵션: 화면에 표시할 요소(HTML, CSS등)
> - data 옵션: 뷰의 반응성(Reactivity)가 반영된 데이터 속성
> - created 옵션: 뷰의 라이프 사이클과 관련된 속성
> - watch 옵션: data에서 정의한 속성이 변화했을 때 추가 동작을 수행할 수 있게 정의
> - methods 옵션: 화면의 동작과 이벤트 로직을 제어하는 메서드
> - computed 옵션: data 속성에 변화가 있을때 자동으로 다시 연산
> ### ■ 인스턴스 라이프 사이클
> ![인스턴스 라이프 사이클](https://kr.vuejs.org/images/lifecycle.png)
> - 인스턴스 라이프 사이클 설명 블로그(https://hyeooona825.tistory.com/40)
## 4일차
### 컴포넌트
> ### ■ 컴포넌트소개
> - 컴포넌트는 화면의 영역을 구분하여 개발할 수 있는 뷰의 기능입니다. 컴포넌트 기반으로 화면을 개발하게 되면 코드의 재사용성이 올라가고 빠르게 화면을 제작할 수 있습니다.
> ![컴포넌트 소개](https://gitlab.com/lucky07.ryan/images/raw/master/vue_component.png)
> ### ■ 컴포넌트 등록
> - Vue.component('컴포넌트 이름', '컴포넌트 내용')
> ### ■ 전역 컴포넌트 등록
> - 모든 인스턴스에서 전역 컴포넌트 사용가능
> ### ■ 지역 컴포넌트 등록
> - 인스턴스 내에서 등록한 컴포넌트만 사용가능
> ### ■ 전역 컴포넌트와 지역 컴포넌트의 차이점
> - 전역 컴포넌트는 플러그인이나 라이브러리를 사용할때 쓰고 지역컴포넌트는 실제 서비스 구현시 대부분 지역 컴포넌트를 사용합
> ### ■ 컴포넌트와 인스턴스와의 관계
> - 인스턴스 생성시 ROOT 컴포넌트 생성
## 5일차
### 컴포넌트 통신 방법 -기본
> ### ■ 컴포넌트 통신
> - 뷰 컴포넌트는 각각 고유한 데이터 유효 범위를 갖습니다. 따라서, 컴포넌트 간에 데이터를 주고 받기 위해선 아래와 같은 규칙을 따라야 합니다.
> ![컴포넌트 통신](https://joshua1988.github.io/vue-camp/assets/img/component-communication.2bb1d838.png)
> - 상위에서 하위로는 데이터를 내려줌, 프롭스 속성
> - 하위에서 상위로는 이벤트를 올려줌, 이벤트 발생
> ### ■ 컴포넌트 통신 규칙이 필요한 이유
> - n방향 통신은 데이터의 흐름을 추적하기 힘듬 따라서 컴포넌트 통신 규칙이 필요
> - 위에서 아래로 데이터 전달 (props 속성으로 전달)
> - 아래에서 위로는 이벤트 전달 (이벤트 emit으로 전달)
> ### ■ props 속성
> - props 속성은 컴포넌트 간에 데이터를 전달할 수 있는 컴포넌트 통신 방법입니다. 
> - props 속성을 기억할 때는 상위 컴포넌트에서 하위 컴포넌트로 내려보내는 데이터 속성으로 기억하면 쉽습니다.
> ### ■ props 속성의 특징
> - 상위 컴포넌트에 데이터를 변경하면 하위 컴포넌트 props 데이터도 같이 변경됨
> ### ■ props 속성 실습
> ### ■ event emit
> - 이벤트 발생은 컴포넌트의 통신 방법 중 하위 컴포넌트에서 상위 컴포넌트로 통신하는 방식입니다.
> ### ■ event emit으로 콘솔 출력하기
> ### ■ event emit 실습
> ### ■ 뷰 인스턴스에서의 this
> - this 관련 글 (https://www.w3schools.com/js/js_this.asp)
## 6일차
### 컴포넌트 통신 방법 -응용
> ### ■ 같은 컴포넌트 레벨 간의 통신 방법
> ![같은 컴포넌트 레벨 간의 통신 방법](https://gitlab.com/lucky07.ryan/images/raw/master/emit.png)
> ### ■ 같은 컴포넌트 레벨 간의 통신 방법 구현 1
> ### ■ 같은 컴포넌트 레벨 간의 통신 방법 구현 2
## 7일차
### 라우터
> ### ■ 뷰 라우터 소개와 설치
> - 뷰 라우터는 뷰 라이브러리를 이용하여 싱글 페이지 애플리케이션을 구현할 때 사용하는 라이브러리입니다.
> #### 뷰 라우터 설치
> - 프로젝트에 뷰 라우터를 설치하는 방법은 CDN 방식과 NPM 방식 2가지가 있습니다.
> ###### CDN 방식
```javascript
<script src="https://unpkg.com/vue-router/dist/vue-router.js">
```
> ###### NPM 방식
```sh
npm install vue-router
```
> - 라우터 공식 사이트 설치 문서 링크(https://router.vuejs.org/installation.html)
> ### ■ 뷰 라우터 인스턴스 연결 및 초기 상태 안내
```javascript
<div id="app"></div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
<script>
    var router = new VueRouter({

    });
    new Vue({
        el: '#app',
        router: router //router 속성: router 변수
    });
</script>
```
> ### ■ routes 속성 설명
```javascript
<script>
    var LoginComponent = {
        template:'<div>login</div>'
    }
    var MainComponent = {
        template:'<div>main</div>'
    }
    var router = new VueRouter({
        // 페이지의 라우팅 정보 (url 매핑 )
        routes: [
            {
                // 페이지의 url 이름
                path: '/login',
                // 해당 url에서 표시될 컴포넌트
                component: LoginComponent
            },
            {
                path: '/main',
                component: MainComponent
            }
        ]
    });
    new Vue({
        el: '#app',
        router: router //router 속성: router 변수
    });
</script>
```
> ### ■ 라우터가 표시되는 영역 및 router-view 태그 설명
> - 브라우저의 주소 창에서 URL이 변경되면, 앞에서 정의한 routes 속성에 따라 해당 컴포넌트가 화면에 뿌려집니다. 
> - 이 때 뿌려지는 지점이 템플릿의 <router-view>입니다.
> - 앞에서 정의한 라우팅 옵션 기준으로 /login은 로그인 컴포넌트를 /main은 홈 컴포넌트를 화면에 표시합니다.
> ### ■ 링크를 이용한 페이지 이동 및 router-link 태그 설명
> - 일반적으로 웹 페이지에서 페이지 이동을 할 때는 사용자가 url을 다 쳐서 이동하지 않습니다.
> - 이 때 화면에서 특정 링크를 클릭해서 페이지를 이동할 수 있게 해줘야 하는데 그게 바로 <router-link> 입니다.
```html
<div>
  <router-link to="/login"></router-link>
</div>
```
## 8일차
### HTTP 통신 라이브러리 - axios
> ### ■ HTTP 라이브러리와 Ajax 그리고 Vue Resource
> - Vue Resource 라이브러리는 2년전까지만에도 Vuejs 공식 라이브러리 였으나 현재는 업데이트가 되지 않고 있어 공식 라이브러리에서 빠짐.
> - Vuejs 커뮤니티에서 Http cilent 통신 라이브러리로 Axios를 많이 사용
> - Ajax 위키백과 링크(https://ko.wikipedia.org/wiki/Ajax)
> - Vue Resource 깃헙 주소(https://github.com/pagekit/vue-resource)
> ### ■ axios 소개 및 오픈 소스를 사용하기 전에 알아야 할 것들
> - 뷰에서 권고하는 HTTP 통신 라이브러리는 액시오스(Axios)입니다. 
> - Promise 기반의 HTTP 통신 라이브러리이며 상대적으로 다른 HTTP 통신 라이브러리들에 비해 문서화가 잘되어 있고 API가 다양합니다.
> - Axios 깃헙 주소(https://github.com/axios/axios)
> - 자바스크립트 비동기 처리와 콜백 함수(https://joshua1988.github.io/web-development/javascript/javascript-asynchronous-operation/)
> - 자바스크립트 Promise 이해하기(https://joshua1988.github.io/web-development/javascript/promise-for-beginners)
> ### ■ axios 실습 및 this 설명
```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <button v-on:click="getData(1)">get user</button>
        <div> {{ users }}</div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        new Vue({
            el:'#app',
            data : {
                users : []
            },
            methods:{
                getData: function(id){
                    const vm = this;
                     axios.get('https://jsonplaceholder.typicode.com/users')
                     .then(function(response){
                        vm.users = response.data;
                     })
                     .catch(function(error){
                        console.log(error );
                     });
                }
            }
        });
    </script>
    
</body>
</html>
```
> ### ■ 웹 서비스에서의 클라이언트와 서버와의 HTTP 통신 구조
> ![웹 서비스에서의 클라이언트와 서버와의 HTTP 통신 구조](https://gitlab.com/lucky07.ryan/images/raw/master/http.png)
## 9일차
### 템플릿 문법 -기본
> ### ■ 템플릿 문법 소개
> - 뷰의 템플릿 문법이란 뷰로 화면을 조작하는 방법을 의미합니다. 
> - 템플릿 문법은 크게 데이터 바인딩과 디렉티브로 나뉩니다.
> #### ○ 데이터 바인딩
> - 데이터 바인딩은 뷰 인스턴스에서 정의한 속성들을 화면에 표시하는 방법입니다. 
> - 가장 기본적인 데이터 바인딩 방식은 콧수염 괄호(Mustache Tag)입니다.
> #### ○ 디렉티브
> - 디렉티브는 뷰로 화면의 요소를 더 쉽게 조작하기 위한 문법입니다
> - 자주 사용되는 디렉티브로 (v-bind, v-on, v-model)
> - 사용자 지정 디렉티브(https://kr.vuejs.org/v2/guide/custom-directive.html)
> ### ■ 데이터 바인딩과 computed 속성
```javascript
<div id="app">{{str}}
    <p>{{num}}</p>
    <p>{{doubleNum}}</p>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    new Vue({
        el: '#app',
        data: {
            str: 'hi',
            num: 10
        },
        computed: {
            doubleNum: function(){
                return this.num *2;
            }
        }
    })
</script>
```
> ### ■ 뷰 디렉티브와 v-bind
> ### ■ 클래스 바인딩, v-if, v-show
> - v-if 조건문으로 true이면 해당 태그를 보여줌
> - v-show도 동일한 기능으로 true이면 해당 태그를 보여줌 다른 점은 v-if는 태그자체를 생성하지 않지만 v-show는 태그는 생성하고 display:none으로 처리함
> ### ■ 모르는 문법이 나왔을 때 공식 문서 보고 해결하는 방법
> - https://vuejs.org 공식 사이트 검색을 통해 문법 확인
> ### ■ methods속성과 v-on 디렉티브를 이용한 키보드, 마우스 이벤트 처리 방법
```javascript
<div id="app">
    <button v-on:click="logText">click me</button>
    <input type="text" v-on:keyup.enter="logText">
</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
        new Vue({
            el: '#app',
            methods: {
                logText: function(){
                    console.log('clicked');
                }
            }
        })
</script>
```
### 템플릿 문법 -실전
> ### ■ watch 속성
> -watch 속성은 특정 데이터의 변화를 감지하여 자동으로 특정 로직을 수행해주는 속성입니다.
```javascript
new Vue({
  data() {
    return {
      message: 'Hello'
    }
  },
  watch: {
    message: function(value, oldValue) {
      console.log(value);
    }
  }
})
```
## 10일차 
### 프로젝트 생성 도구 Vue Cli
> ### ■ Vue Cli 공식 사이트 (https://cli.vuejs.org)
> #### ○ Vue Cli 설치 전 확인 사항
> - node.js v10 이상 설치필요
> #### ○ 설치 방법
> - npm install -g @vue/cli
> #### ○ vue cli 프로젝트 생성 밥업
> - vue create vue-todo
> #### ○ vue cli 로컬 프로젝트 실행 방법
> - npm run serve
## 11일차
## Todo List
### ■ 컴포넌트 설계
> - TodoHeader, TodoInput, TodoList, TodoFooter 4개 컴포넌트로 구성
### ■ 각 컴포너트에서 기능 구현
> - TodoHeader 헤더 정보 노출
> - TodoInput 할일 로컬스토리지에 저장
> - TodoList 할일 로컬스토리지에서 가져와 목록 구현
> - TodoFooter 로컬 스토리지 ClearAll 기능 구현
## 12일 차
### 애클리케이션 구조 개선
#### ■ [리팩토링] 할 일 목록 표시 기능
> - props를 활용해 목록 표시
#### ■ [리팩토링] 할 일 추가 기능
> - emit이벤트 활용해 할 일 추가
#### ■ [리팩토링] 할 일 삭제 기능
> - emit이벤트 활용해 할 일 추가
#### ■ [리팩토링] 할 일 완료 기능
> - emit이벤트 활용해 할 일 추가
#### ■ [리팩토링] 할 일 모두 삭제 기능
> - emit이벤트 활용해 할 일 추가
## 13일차
### 사용자 경험 개선
#### ■ Modal 팝업 적용
> - https://vuejs.org/v2/examples/modal.html
#### ■ Slot
> - https://kr.vuejs.org/v2/guide/components-slots.html
#### ■ Transition
> - https://vuejs.org/v2/guide/transitions.html#ad
## 14일차
## 자바스크립트란
- 자바스크립트 관련 정리 잘된 블로그 (https://poiemaweb.com/js-introduction)
## ES6 배경과 Babel 소개
### ES6란?
- ECMAScript 2015와 동일한 용어
- 2015년은 ES5(2009년)이래로 진행한 첫 메이저 업데이트가 승인된 해
- 최신 Front-End Framework인 React, Angular, Vue에서 권고하는 언어 형식
- ES5에 비해 문법이 간결해져서 익숙해지면 코딩을 훨씬 편하게 할 수 있음
### Babel
- 구 버전 브라우저 중에서는 ES6의 기능을 지원하지 않는 브라우저가 있으므로 transpiling이 필요
- ES6의 문법을 각 브라우저의 호환 가능한 ES5로 변환하는 트랜스파일러
- 바벨 공식 사이트 (https://babeljs.io/)
## const & let 소개
### const & let 새로운 변수 선언 방식
- 블록 단위 {}로 변수의 범위가 제한되었음
- const: 한번 선언한값에대해서 변경할수 없음(상수 개념)
- let: 한번 선언한 값에 대해서 다시 선언할 수 없음
## 변수 스코프와 호이스팅
### ES5특징 - 변수의 Scope
- 기존 자바스크립트(ES5)는 {}에 상관없이 스코프가 설정됨
```javascript
var sum = 0;
for (var i = 1; i <= 5; i++) {
    sum = sum +i;
}
console.log(sum);
console.log(i);
```
### ES5 특징 - Hoisting
- Hoisting이란 선언한 함수와 변수를 해석기가 가장 상단에 있는 것처럼 인식한다.
- js해석기는 코드의 라인 순서와 관계 없이 함수선언식과 변수를 위한 메모리 공간을 먼저 확보한다.
- 따라서, function a() 와 var 는 코드의 최상단으로 끌어 올려진 것(hoisted)처럼 보인다.
```javascript
function willBeOveridden() {
    return 10;
}
willBeOveridden();
function willBeOveridden() {
    return 5;
}
```
```javascript
// hoisting 전
var sum = 5;
sum = sum + i;
function sumAllNumbers() {
 // ...
}
var i = 10;
```
```javascript
// #1 - 함수 선언식과 변수 선언을 hoisting
var sum;
function sumAllNumbers() {
 // ...
}
var i;
// #2 - 변수 대입 및 할당
sum = 5;
sum = sum +i;
i = 10;
```
### ES6 - {} 단위로 변수의 범위가 제한됨
```javascript
let sum = 0;
for(let i = 1; i <= 5; i++) {
    sum = sum +i;
}
console.log(sum); //10
console.log(i); // Uncaught ReferenceError: i is not defined
```
### ES6  - const로 지정한 값 변경 불가능
```javascript
const a = 10;
a = 20; // Uncaught TypeError: Assignment to constant variable
```
하지만, 객체나 배열의 내부는 변경할 수 있다.
```javascript
const a = {};
a.num = 10;
console.log(a); //{num: 10}

conat a = [];
a.push(20);
console.log(a); // [20]
```
### ES6 - let 선언한 값에 대해서 다시 선언 불가능
```javascript
let a = 10;
let a = 20; // Uncaught SyntaxError: Identifier 'a' has already been declared
```
## 화살표 함수 소개 및 설명
### Arrow Function - 화살표 함수
- 함수를 정의할 때 function 이라는 키워드를 사용하지 않고 => 로 대체
- 흔희 사용하는 콜백 함수의 문법을 간결화
```javascript
// ES5 함수 정의 방식
var sum = function(a, b) {
    return a + b;
};

// ES6 함수 정의 방식
var sum = (a, b) => {
    return a + b;
}

sum(10, 20);
```
## 향상된 객체 리터럴(Enhanced Object Literal)
- 객체의 속성을 메서드로 사용할 때 function 예약어를 생략하고 생성 가능
```javascript
var dictionary = {
    words: 100,
    // ES5
    lookup: function() {
        console.log("find words");
    },
    // ES6
    lookup() {
        console.log("find words");
    }
};
```
- 객체의 속성명과 값 명이 동일할 때 아래와 같이 축약 가능
```javascript
var figures = 10;
var dictionary = {
    //figures: figures,
    figures
}
```
## Modules
### 자바스크립트 모듈화 방법
- 자바스크립트 모듈 로더 라이브러리(AMD, Commons JS)기능을 js 언어 자체에서 지원
- 호출되기 전까지는 코드 실행과 동작을 하지 않는 특징이 있음
```javascript
// libs/math.js
export function sum(x, y) {
    return x + y;
}
export var pi = 3.141593;

//main.js
import {sum} from 'libs/math.js';
sum(1,2);
```
-Vue.js에서 마주칠 default export
```javascript
//util.js
export default function(x) {
    return console.log(x);
}

//main.js
import util form 'util.js';
console.log(util);
util("hi");

// app.js
import log form 'util.js';
console.log(log);
log("hi");
```
## 참고 문서
- es6 관련 블로그 링크 (https://blog.asamaru.net/2017/08/14/top-10-es6-features/)
## 15일차
### Vuex
### ■ Vuex란?
> - 무수히 많은 컴포넌트의 데이터를 관리하기 위한 상태 관리 패턴이자 라이브러리
> - React의 Flux 패턴에서 기인함
> - Vue.js 중고급 개발자로 성장하기 위한 필수 관문
### ■ Flux란?
> ○ MVC 패턴의 복잡한 데이터 흐름 문제를 해결하는 개발 패턴 - Unidirectional data flow(단반향 흐름)
> ![FLUX](https://gitlab.com/lucky07.ryan/images/raw/master/flux.png)
> 1. action: 화면에서 발생하는 이벤트 또는 사용자의 입력
> 2. dispatcher: 데이터를변경하는 방법, 메소드
> 3. model: 화면에 표시할 데이터
> 4. view: 사용자에게 비춰지는 화면
### MVC패턴과 Flux패턴 비교
> 1. MVC패턴
> ![MVC패턴](https://gitlab.com/lucky07.ryan/images/raw/master/mvc.png)
> 2. Flux 패턴
> ![FLUX](https://gitlab.com/lucky07.ryan/images/raw/master/flux.png)
### MVC 패턴의 문제점
> - 기능 추가 및 변경에 따라 생기는 문제점을 예측할 수가 없음. (예)페이스북 채팅 화면
> - 앱이 복잡해지면서 생기는 업데이트 루프
> ![MVC 패턴 문제점](https://gitlab.com/lucky07.ryan/images/raw/master/mvc%E1%84%91%E1%85%A2%E1%84%90%E1%85%A5%E1%86%AB_%E1%84%86%E1%85%AE%E1%86%AB%E1%84%8C%E1%85%A6%E1%84%8C%E1%85%A5%E1%86%B7.png)
### Vuex가 왜 필요할까?
> - 복잡한 애플리케이션에서 컴포넌트의 개수가 많아지면 컴포넌트간에 데이터 전달이 어려워진다.
> ![Vuex Why](https://gitlab.com/lucky07.ryan/images/raw/master/vuex_why.png)
### 이벤트 버스로 해결?
> - 어디서 이벤트를 보냈는지 혹은 어디서 이벤트를 받았는지 알기 어려움
```javascript
// Login.vue
eventBus.$emit('fetch', loginInfo);

// List.vue
eventBus.$on('display', data => this.displayOnScreen(data));

// Chart.vue
eventBus.$emit('refreshData', chartData);
```
> - 컴포넌트 간 데이터 전달이 명시적이지 않음
### Vuex로 해결할 수 있는 문제
> 1. MVC패턴에서 발생하는 구조적 오류
> 2. 컴포넌트 간 데이터 전달 명시
> 3. 여러 개의 컴포넌트에서 같은 데이터를 업데이트 할 때 동기화 문제
### Vuex 컨셉
> - State: 컴포넌트 간에 공유하는 데이터 data()
> - View: 데이터를 표시하는 화면 template
> - Action: 사용자의 입력에 따라 데이터를 변경하는 methods
> ![Vuex Flow](https://gitlab.com/lucky07.ryan/images/raw/master/vuex_flow.png)
> - 단방향 데이터 흐름 처리를 단순하게 도식화한 그림
### Vuex 구조
> - 컴포넌트 -> 비동기 로직 -> 동기 로직 -> 상태
> - ![Vuex 구조](https://gitlab.com/lucky07.ryan/images/raw/master/vues_%E1%84%80%E1%85%AE%E1%84%8C%E1%85%A9%E1%84%83%E1%85%A9.png)
### Vuex 설치하기
> - Vuex는 싱글 파일 컴포넌트 체계에서 NPM 방식으로 라이브러리를 설치하는게 좋다.
```javascript
npm install vuex --save
```
> - ES6와 함께 사용해야 더 많은 기능과 이점을 제공받을 수 있다.
### Vuex 시작하기
> - src 폴더 밑에 store폴더를 만들고 store.js 파일 생성
```javascript
import Vue from 'vue'
import Vuex from 'vuex'

export const store = new Vuex.Store({
    // ...
});
```
### Vuex 기술 요소
> - state: 여러 컴포넌트에 공유되는 데이터 data
> - getters: 연산된 state값을 접근하는 속성 computed
> - mutations: state 값을 변경하는 이벤트 로직 메서드 methods
> - actions: 비동기 처리 로직을 선언하는 메서드 aysnc methods
### state란?
> - 여러 컴포넌트 간에 공유할 데이터 - 상태
```javascript
// Vue
data: {
    message: 'Hello Vue.js!'
}

// Vuex
state: {
    message: 'Hello Vue.js!'
}
```
```javascript
<!-- Vue -->
<p>{{ message }}</p>

<!-- Vuex -->
<p>{{ this.$store.state.message }}</p> 
```
### getters란?
> - state 값을 접근하는 속성이자 computed()처럼 미리 연산된 값을 접근하는 속성
```javascript
// store.js
state: {
    num: 10
},
getters: {
    getNumber(state) {
        return state.num;
    },
    doubleNumber(state) {
        return state.num * 2;
    }
}
```
```javascript
<p>{{ this.$store.getters.getNumber }}</p>
<p>{{ this.$store.getters.doubleNumber }}</p>
```
### mutations란?
> - state의 값을 변경할 수 있는 유일한 방법이자 메서드
> - 뮤테이션은 commit()으로 동작시킨다.
```javascript
// store.js
state: { num: 10 },
mutations: {
    printNumbers(state) {
        return state.num;
    }
    sumNumbers(state, anotherNum) {
        return state.num + anotherNum;
    }
}
    
// App.vue
this.$store.commit('printNumbers');
this.$store.commit('sumNumbers', 20);
```
### mutations의 commit()형식
> - state를 변경하기 위해 mutations를 동작시킬 때 인자(payload)를 전달할 수 있음
```javascript
// store.js
state: { storeNum: 10 },
mutations: {
    modifyState(state, payload) {
        console.log(payload.str);
        return state.storeNum += payload.num;
    }
}

// App.vue
this.$store.commit('modifyState', {
    str: 'passwd from payload',
    num: 20
});
```
### state는 왜 직접 변경하지 않고 mutations로 변경할까?
> - 여러 개의 컴포넌트에서 아래와 같이 state값을 변경하는 경우 어느 컴포넌트에서 해당 state를 변경했는지 추적하기가 어렵다.
```javascript
methods: {
    increaseCounter(){ this.$store.state.counter++; }
}
```
> - 특정 시점에 어떤 컴포넌트가 state를 접근하여 변경한 건지 확인하기 어렵기 때문
> - 따라서, 뷰의 반응성을 거스르지 않게 명시적으로 상태 변화를 수행, 반응성, 디버킹, 테스팅 혜택
### actions란?
> - 비동기 처리 로직을 선언하는 메서드, 비동기 로직을 담당하는 mutations
> - 데이터 요청, Promise, ES6 async과 같은 비동기 처리는 모두 actions에 선언
```javascript
// store.js
state: {
    num: 10
},
mutations: {
    doubleNumber(state) {
        state.num * 2;
    }
},
actions: {
    delayDoubleNumber(context) { // context로 store의 메소드와 속성 접근
        context.commit('doubleNumber');
    }
}
// App.vue
this.$store.dispatch('delayDoubleNumber');
```
### actions 비동기 코드 예제 1
```javascript
// store.js
mutations: {
    addCounter(state) {
        state.counter++
    }
},
actions: {
    delayedAddCounter(context) {
        setTimeout(() => context.commit('addCounter'), 2000);
    }
}

// App.vue
methods: {
    incrementCounter() {
        this.$store.dispatch('delayedAddCounter');
    }
}
```
### actions 비동기 코드 예제 2
```javascript
// store.js
mutations: {
    setData(state, fetchedData) {
        state.product = fetchedData;
    }
},
actions: {
    fetchProductData(context) {
        return axios.get('https://domain.com/products/1')
                    .then(response => context.commit('setData', response));
    }
}

// App.vue
methods: {
    getProduct() {
        this.$store.dispatch('fetchProductData');
    }
}
```
## 16일차 (헬퍼 함수 및 ES6 Spread 연산자 소개, Vuex 프로젝트 구조화 및 모듈화)
### 헬퍼의 사용법
> - 헬퍼를 사용하고자 하는 vue 파일에서 아래와 같이 해당 헬퍼를 로딩
```javascript
// App.vue
import { mapState } from 'vuex'
import { mapGetters } from 'vuex'
import { mapMutations } from 'vuex'
import { mapActions } from 'vuex'

export default {
    computed() {
        ...mapState(['num']),
        ...mapGetters(['countedNum'])
    },
    methods: {
        ...mapMutations(['clickBtn']),
        ...mapActions(['asyncClickBtn'])
    }
}
```
> - ... 은 ES6 의 Object Spread Operator 입니다.
### 각 속성들을 더 쉽게 사용하는 방법 - Helper
#### Store에 있는 아래4가지 속성들을 간편하게 코딩하는 방법
> - state -> mapState
> - getters -> mapGetters
> - mutations -> mapMutations
> - actions -> mapActions
### mapState
> - Vuex에 선언한 state 속성을 뷰 컴포넌트에 더 쉽게 연결해주는 헬퍼
```javascript
// App.vue
import { mapState } from 'vuex'

computed() {
    ...mapState(['num'])
    // num() { return this.$store.state.num' }
}

// store.js
state: {
    num: 10
}
```
```javascript
<!-- <p>{{ this.$store.state.num }}</p> -->
<p>{{ this.num }}</p>
```
### mapGetters
> - Vuex에 선언한 getters 속성을 뷰 컴포넌트에 더 쉽게 연결해주는 헬퍼
```javascript
// App.vue
import { mapGetters } from 'vuex'

computed() { ...mapGetters(['reverseMessage']) } // ... 쓰는 이유 computed 속성과 mapGetters속성을 같이 쓰기위해

// store.js
getters: {
    reverseMessage(state) {
        return state.msg.split('').reverse().join('');
    }
}
```
```javascript
<!-- <p>{{ this.$store.getters.reverseMessage }} </p> -->
<p>{{ this.reverseMessage }}</p>
```
### mapMutations
> - Vuex에 선언한 mutations 속성을 뷰 컴포넌트에 더 쉽게 연결해주는 헬퍼
```javascript
// App.uve
import { mapMutations } from 'vuex'

methods: {
    ...mapMutations(['clickBtn']),
    authLogin(){},
    displayTable() {}
}

// store.js
muetaions: {
    clickBtn(state) {
        alert(state.msg);
    }
}
```
```javascript
<button @click="clickBtn">popup message</button>
```
### mapActions
> - Vuex에 선언한 actions 속성을 뷰 컴포넌트에 더 쉽게 연결해주는 헬퍼
```javascript
// App.vue
import { mapActions } from 'vuex'

methods: {
    ...mapActions(['delayClickBtn'])
}

// store.js
actions: {
    delayClickBtn(context) {
        setTimeout(() => context.commit('clickBtn'), 2000);
    }
}
```
```javascript
<button @click="delayClickBtn">delay popup message</button>
```
### 헬퍼의 유연한 문법
> - Vuex에 선언한 속성을 그대로 컴포넌트에 연결하는 문법
```javascript
// 배열 리터럴
...mapMutations([
    'clickBtn', // 'clickBtn' : clickBtn
    'addNumber' // addNumber(인자)
])
```
> - Vuex에 선언한 속성을 컴포넌트의 특정 메서드에다가 연결하는 문법
```javascript
// 객체 리터럴
...mapMutations({
    popupMsg: 'clickBtn' // 컴포넌트 메소드 명 : Store의 뮤테이션 명
})
```
### 프로젝트 구조화와 모듈화 방법1
> - 아래와 같은 store 구조를 어떻게 모듈화 할 수 있을까?
```javascript
// store.js
import Vue from 'vue'
import Vuex from 'vuex'

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {}
});
```
> - ES6의 Import & Export를 이용하여 속성별로 모듈화
```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from 'store/getters.js'
import * as mutations from 'store/mutations.js'
import * as actions from 'store/actions.js'

export const store = new Vuex.Store({
    state: {},
    getters: getters
    mutations: mutations,
    actions: actions
});
```
### 프로젝트 구조화와 모듈화 방법2
> - 앱이 비대해져서 1개의 store로는 관리가 힘들 때 modules 속성 사용
```javascript
// store.js
import Vue from 'vue'
import Vuex from 'vuex'
import todo from 'modules/todo.js'

exrpot const store = new Vuex.Store({
    modules: {
        moduleA: todo, // 모듈 명칭 : 모듈 파일명
        todo // todo: todo
    }
)};

// todo.js
const state = {}
const getters = {}
const mutations = {}
const actions = {}
```